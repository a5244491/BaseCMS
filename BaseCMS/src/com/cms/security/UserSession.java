package com.cms.security;

public class UserSession {

	private String addr;

	private String sessid;

	private String username;
	
	private boolean isModifyPwd;

	public String getAddr() {
		return addr;
	}

	public void setAddr(String addr) {
		this.addr = addr;
	}

	public String getSessid() {
		return sessid;
	}

	public void setSessid(String sessid) {
		this.sessid = sessid;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public boolean isModifyPwd() {
		return isModifyPwd;
	}

	public void setModifyPwd(boolean isModifyPwd) {
		this.isModifyPwd = isModifyPwd;
	}

}
