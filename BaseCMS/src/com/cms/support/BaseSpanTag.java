package com.cms.support;

import java.io.IOException;
import java.util.List;

import javax.servlet.jsp.JspException;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cms.entity.DataDict;

/**
 * @author: zhangp Date: 14-9-17 Time: 上午9:55.
 */
public class BaseSpanTag extends BaseDiyTag {
	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = LoggerFactory
			.getLogger(BaseSpanTag.class);

	private String selectValue;

	private StringBuilder sb ;
	public int doStartTag() throws JspException {
		sb = new StringBuilder();
		sb.append("<span name='").append(name).append("'");
		generateAttribute(sb);// 加入属性
		sb.append(">");
		try {
			this.pageContext.getOut().print(sb.toString());
		} catch (IOException e) {
			LOGGER.error("类（BaseDiyTag）的 方法（doStartTag）异常" + e);
		}
		return EVAL_BODY_INCLUDE;
	}
	
	@Override
	public int doEndTag() throws JspException {
		List<DataDict> dataDicts = dataDictService.listDataByName(collection);
		sb = new StringBuilder();
		for (DataDict dataDict : dataDicts) {
			if(StringUtils.isNotBlank(selectValue)){
				String[] selectValueArray = selectValue.split(",");
				for(int i=0;i<selectValueArray.length;i++){
					if(i==selectValueArray.length-1){
						if (StringUtils.isNotBlank(selectValueArray[i]) && selectValueArray[i].equals(dataDict.getValue())) {
							sb.append(dataDict.getDescription());
						}
					}else{
						if (StringUtils.isNotBlank(selectValueArray[i]) && selectValueArray[i].equals(dataDict.getValue())) {
							sb.append(dataDict.getDescription()+",");
						}
					}
					
				}
			}
			
		}
		sb.append("</span>");
		try {
			this.pageContext.getOut().print(sb.toString());
		} catch (IOException e) {
			LOGGER.error("类（BaseDiyTag）的 方法（doEndTag）异常" + e);
		}
		return super.doEndTag();
	}
	public String getSelectValue() {
		return selectValue;
	}

	public void setSelectValue(String selectValue) {
		this.selectValue = selectValue;
	}

	public StringBuilder getSb() {
		return sb;
	}

	public void setSb(StringBuilder sb) {
		this.sb = sb;
	}

}
