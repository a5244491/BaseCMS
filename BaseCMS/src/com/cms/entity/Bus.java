package com.cms.entity;

import java.text.ParseException;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.cms.common.hibernate.BaseEntity;

@Entity
@Table(name = "t_cms_bus")
public class Bus extends BaseEntity {
	private static final long serialVersionUID = 1L;
	@Column
	private String createUser;
	@Column
	private Integer status;// 0正常，1待确认，2关闭
	@Column
	private String fromAddr;
	@Column
	private String toAddr;
	@Column
	private String goDate;
	@Column
	private Double price;
	@Column
	private Integer seats;
	@Column
	private String remark;

	@Transient
	public String getStatusName() throws ParseException {
		String statusName = "待抢单";
		if (status != null) {
			if (status == 1) {
				statusName = "已抢单";
			}
			if (status == 2) {
				statusName = "关闭";
			}
		}

		return statusName;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getFromAddr() {
		return fromAddr;
	}

	public void setFromAddr(String fromAddr) {
		this.fromAddr = fromAddr;
	}

	public String getToAddr() {
		return toAddr;
	}

	public void setToAddr(String toAddr) {
		this.toAddr = toAddr;
	}

	public String getGoDate() {
		return goDate;
	}

	public void setGoDate(String goDate) {
		this.goDate = goDate;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	/**
	 * @return the seats
	 */
	public Integer getSeats() {
		return seats;
	}

	/**
	 * @param seats
	 *            the seats to set
	 */
	public void setSeats(Integer seats) {
		this.seats = seats;
	}

	/**
	 * @return the remark
	 */
	public String getRemark() {
		return remark;
	}

	/**
	 * @param remark
	 *            the remark to set
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Bus [id=" + id + ", createUser=" + createUser + ", createDate="
				+ createDate + ", status=" + status + ", fromAddr=" + fromAddr
				+ ", toAddr=" + toAddr + ", goDate=" + goDate + ", price="
				+ price + ", seats=" + seats + ", remark=" + remark + "]";
	}

}
