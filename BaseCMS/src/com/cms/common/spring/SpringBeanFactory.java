package com.cms.common.spring;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public abstract class SpringBeanFactory {
	private static final String SPRING_CONFIG_NAME	= "applicationContext*.xml";	//可带*
	private static ApplicationContext ctx			= null;
	
	private static void init() {
		if (ctx == null) {
			ctx = new ClassPathXmlApplicationContext(SPRING_CONFIG_NAME);
		}
	}
	
	public static Object getBean(String beanName){
		init();
		return ctx.getBean(beanName);
	}
	
	public static <T> T getBean(String beanName, Class<T> requiredType) {
		init();
		return ctx.getBean(beanName, requiredType);
	}
	
	public static List<Object> getBeans(List<String> names) {
		List<Object> os = new ArrayList<Object>();
		for (String name : names) {
			os.add(getBean(name));
		}
		return os;
	}
	
	public static <T> List<T> getBeans(List<String> names, Class<T> requiredType) {
		List<T> ts = new ArrayList<T>();
		for (String name : names) {
			ts.add(getBean(name, requiredType));
		}
		return ts;
	}
	
}
