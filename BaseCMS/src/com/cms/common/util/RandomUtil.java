package com.cms.common.util;

import java.util.Random;
import java.util.UUID;

/** 
 * 随机数、随机UUID.
 * 
 * <pre>
 * 此类未提供的方法可以使用RandomStringUtils
 * //随机count位数字
 * RandomStringUtils.randomNumeric(count)
 * //随机count位字母
 * RandomStringUtils.randomAlphabetic(count)
 * //随机count位字母+数字
 * RandomStringUtils.randomAlphanumeric(count)
 * //随机count位Ascii码
 * RandomStringUtils.randomAscii(count)
 * </pre>
 */
public abstract class RandomUtil {

	private static Random random = new Random();
	
	/** 获取随机整数. */
	public static int nextInt() {
		return random.nextInt();
	}
	
	/** 获取随机正整数. */
	public static int nextIntAbs() {
		return Math.abs(nextInt());
	}
	
	/** 获取限定范围随机整数(0-[max-1]). */
	public static int nextInt(int max) {
        return random.nextInt(max);
	}
	
	/** 随机获取UUID字符串(无中划线). */
	public static String getUUID() {
		String uuid = UUID.randomUUID().toString();
		return uuid.replace("-","");
	}

	/** 已知事件促发概率(r=0-100)，根据概率随机返回是否促发(true-促发、false-未促发). */
	public static boolean checkRate(int r){
		int n = nextInt(100);	//0-99
		return n < r ? true : false;
	}
	
}