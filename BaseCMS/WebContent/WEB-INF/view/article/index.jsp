<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="/tld/html-tags" prefix="html"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@ include file="/WEB-INF/view/head.jsp"%>
</head>
<body class="gray-bg">
	<div class="wrapper wrapper-content animated fadeInRight">
		<div class="row">
			<div class="col-sm-12">
				<div class="ibox float-e-margins">
					<div class="ibox-title">
						<form class="form-inline form-index" action="#" method="post" id="form">
							<div class="form-group">
								<label class="control-label">标题</label> <input name="title"
									id="title" type="text" maxlength="20" class="form-control" />
							</div>
							<div class="form-group">
								<label class="control-label">公众号</label> <input name="wxAccount"
									id="wxAccount" type="text" maxlength="20" class="form-control" />
							</div>
							<div class="form-group">
								<label class="control-label">所属栏目</label> 
								<select name="catgoryId" id="catgoryId" class="form-control">
									<option value="">全部</option>
									<c:forEach items="${catgory }" var="item">
										<option value="${item.id }">${item.name }</option>
									</c:forEach>
								</select>
							</div>
							<div class="form-group">
								<label class="control-label">摘要</label> <input name="remark"
									id="remark" type="text" maxlength="20" class="form-control" />
							</div>
							<div class="form-group">
								<label class="control-label">内容</label> <input name="content"
									id="content" type="text" maxlength="20" class="form-control" />
							</div>
							<div class="form-group">
								<label class="control-label">状态</label>
								<html:select cssClass="form-control" collection="articleStatus"
									selectValue="${article.status }" name="status" id="status">
									<option value="">全部</option>
								</html:select>
							</div>
							<div class="form-group">
								<label class="control-label">更新时间</label> <input id="startDate"
									name="startDate" class="laydate-icon form-control layer-date">
								- <input id="endDate" name="endDate"
									class="laydate-icon form-control layer-date">
							</div>

							<div class="form-group button-group" style="width: 100%;">

								<html:auth res="article/list">
									<input onclick="reload();" type="button" id="search" value="查询"
										class="btn btn-default">
								</html:auth>
								<html:auth res="article/add">
									<input type="button" id="new" value="新增"
										class="btn btn-default" onClick="add();">
								</html:auth>
								<input type="reset" id="reset" value="重置"
									class="btn btn-default">
							</div>
						</form>
					</div>
					<div class="ibox-content">
						<table id="data"
							class="table table-striped table-bordered table-hover dataTables-example">
							<thead>
								<tr>
									<th>编号</th>
									<th>栏目</th>
									<th>标题</th>
									<th>公众号</th>
									<th>状态</th>
									<th>更新时间</th>
									<th>操作</th>
								</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		var table;
		function reload() {
			table.fnDraw();
		}
		var start = {
			elem : "#startDate",
			format : "YYYY-MM-DD 00:00:00",
			max : laydate.now(),
			istime : false,
			istoday : true,
			choose : function(datas) {
				end.min = datas;
				end.start = datas;
			}
		};
		var end = {
			elem : "#endDate",
			format : "YYYY-MM-DD 23:59:59",
			min : laydate.now(),
			max : "2099-06-16 23:59:59",
			istime : false,
			istoday : true,
			choose : function(datas) {
				start.max = datas
			}
		};
		laydate(start);
		laydate(end);
		$(document)
				.ready(
						function() {
							table = $("#data")
									.dataTable(
											{
												"bLengthChange" : false, //改变每页显示数据数量 
												"searching" : false,
												"bProcessing" : false, // 是否显示取数据时的那个等待提示
												"bServerSide" : true,//这个用来指明是通过服务端来取数据
												"sAjaxSource" : "article/list",//这个是请求的地址
												"fnServerData" : retrieveData,
												"columns" : [ {
													"data" : "id",
													"defaultContent" : ""
												}, {
													"data" : "catgoryId",
													"defaultContent" : ""
												}, {
													"data" : "title",
													"defaultContent" : ""
												}, {
													"data" : "wxAccount",
													"defaultContent" : ""
												}, {
													"data" : "status",
													"defaultContent" : ""
												}, {
													"data" : "modifyDate",
													"defaultContent" : ""
												} ],
												"fnServerParams" : function(aoData) {
													aoData.push({
														"name" : "title",
														"value" : $("#title")
																.val()
													});
													aoData.push({
														"name" : "status",
														"value" : $("#status")
																.val()
													});
													aoData.push({
														"name" : "startDate",
														"value" : $(
																"#startDate")
																.val()
													});
													aoData.push({
														"name" : "endDate",
														"value" : $("#endDate")
																.val()
													});
													aoData.push({
														"name" : "content",
														"value" : $("#content")
																.val()
													});
													aoData.push({
														"name" : "wxAccount",
														"value" : $("#wxAccount")
																.val()
													});
													aoData.push({
														"name" : "remark",
														"value" : $("#remark")
																.val()
													});
													aoData.push({
														"name" : "catgoryId",
														"value" : $("#catgoryId").val()
													});
												},
												"columnDefs" : [
														// 将name列变为红色
														{
															"targets" : [ 0 ], // 目标列位置，下标从0开始
															"render" : function(
																	data, type,
																	full) { // 返回自定义内容
																return "<span style='color:red;'>"
																		+ data
																		+ "</span>";
															}
														},{
															"targets" : [ 1 ], 
															"data":"catgoryId",// 目标列位置，下标从0开始
															"render" : function(
																	data, type,
																	full) { // 返回自定义内容
																 var dataCn="";
																	var jsonObj=eval("("+'${catgoryJSON}'+")");  
															        $.each(jsonObj, function (i, item) {  
															           if(item.id==data){
															        	   dataCn = item.name;
															           }  
															        }); 
																	return dataCn;
															}
														},
														{
															"targets" : [ 4 ], // 目标列位置，下标从0开始
															"data" : "status", // 数据列名
															"render" : function(
																	data, type,
																	full) { // 返回自定义内容
															    var dataCn="";
																var jsonObj=eval("("+'${dataDict}'+")");  
														        $.each(jsonObj, function (i, item) {  
														           if(item.name=="articleStatus"&&item.value==data){
														        	   dataCn = item.description;
														           }  
														        }); 
																return dataCn;
															}
														},
														// 增加一列，包括删除和修改，同时将需要传递的数据传递到链接中
														{
															"targets" : [ 6 ], // 目标列位置，下标从0开始
															"data" : "id", // 数据列名
															"render" : function(
																	data, type,
																	full) { // 返回自定义内容
																var modify = '<html:auth res="article/modify">'
																		+ '<a href="#" onclick="modify(\''
																		+ data
																		+ '\')">修改</a></html:auth>';
																var del = '<html:auth res="article/delete">'
																		+ '<a href="#" onclick="del(\''
																		+ data
																		+ '\')">删除</a></html:auth>';
																var view = '<html:auth res="article/view">'
																		+ '<a href="#" onclick="view(\''
																		+ data
																		+ '\')">详情</a></html:auth>';
																return del
																		+ "&nbsp;"
																		+ modify
																		+ "&nbsp;"
																		+ view;
															}
														} ]
											// 获取数据的处理函数
											});
						});

		// 3个参数的名字可以随便命名,但必须是3个参数,少一个都不行
		function retrieveData(url, data, fnCallback) {
			$.ajax({
				url : url,//这个就是请求地址对应sAjaxSource
				data : {
					"iDisplayLength" : data.iDisplayLength,
					"iDisplayStart" : data.iDisplayStart,
					"title" : data.title,
					"content" : data.content,
					"wxAccount" : data.wxAccount,
					"status" : data.status,
					"startDate" : data.startDate,
					"endDate" : data.endDate,
					"remark" : data.remark,
					"catgoryId" : data.catgoryId
				},//这个是把datatable的一些基本数据传给后台,比如起始位置,每页显示的行数
				type : 'post',
				dataType : 'json',
				success : function(result) {
					fnCallback(result);//把返回的数据传给这个方法就可以了,datatable会自动绑定数据的
				},
				error : function(msg) {
				}
			});
		}

		function add() {
			var index = layer.open({
				type : 2,
				title : "文章新增",
				area : [ '800px', '400px' ],
				fix : false, //不固定
				maxmin : true,
				content : 'article/add'
			});
			layer.full(index);
		}
		function modify(id) {
			var index = layer.open({
				type : 2,
				title : "文章修改",
				area : [ '800px', '400px' ],
				fix : false, //不固定
				maxmin : true,
				content : 'article/modify?id=' + id
			});
			layer.full(index);
		}
		function del(id) {
			//询问框
			layer.confirm('确定要删除么？', {
				btn : [ '确定', '取消' ]
			//按钮
			}, function() {
				$.ajax({
					url : "article/delete",
					data : {
						id : id
					},
					type : 'post',
					contentType : 'application/x-www-form-urlencoded',
					encoding : 'UTF-8',
					cache : false,
					success : function(result) {
						if (result.success) {
							layer.alert('操作成功');
							reload();
						} else {
							layer.alert('操作失败,' + result.obj);
						}
					},
					error : function() {
						layer.alert('系统异常');
					}
				});
			}, function() {
				//取消动作
			});
		}
		function view(id) {
			var index = layer.open({
				type : 2,
				title : "用户详情",
				area : [ '800px', '400px' ],
				fix : false, //不固定
				maxmin : true,
				content : 'article/view?id=' + id
			});
			layer.full(index);
		}
	</script>
</body>
</html>