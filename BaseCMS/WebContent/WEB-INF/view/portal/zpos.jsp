<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();    
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>卡卡付</title>
<%@ include file="/WEB-INF/view/portal/head.jsp"%>
<link rel="stylesheet" href="<c:url value="/css/portal/zpos.css"/>"/>
</head>

<body>
	<div class="top">
		<div class="container"><img src="<c:url value="/images/portal/one/ikkpay_1.png"/>" /></div>
	</div>
	<img src="<c:url value="/images/portal/kklPay/kklpay_banner.png"/>" />
	<div class="container">
		<div class="bigTitle">专业、贴心的支付服务</div>
		<div class="row">
			<div class="col-md-3">
				<img src="<c:url value="/images/portal/kklPay/Bluetooth.png"/>" />
				<div class="title">蓝牙接入，硬件安全品质保证</div>
				<div class="content" style="text-align:left;">通过中国人民银行PBOC1.0、PBOC2.0认证，银行卡检测中心认证、PIN输入设备安全监测规范认证。</div>
			</div>
			<div class="col-md-3">
				<img src="<c:url value="/images/portal/kklPay/moreCard.png"/>" />
				<div class="title">多系统集成，多卡片支持</div>
				<div class="content" style="text-align:left;">IOS、Android手机支持芯片卡、磁条卡。</div>
			</div>
			<div class="col-md-3">
				<img src="<c:url value="/images/portal/kklPay/service.png"/>" />
				<div class="title">贴身收款管家24小时服务</div>
				<div class="content" style="text-align:left;">移动POS机，及时开通，实时收款及时到账，到账稳定，费率优惠。</div>
			</div>
			<div class="col-md-3">
				<img src="<c:url value="/images/portal/kklPay/pay.png"/>" />
				<div class="title">丰富的支付方式选择</div>
				<div class="content" style="text-align:left;">NFC支付，无卡支付、蓝牙POS支付。</div>
			</div>
		</div>
		<div class="bigTitle">移动互联支付新方式</div>
		<div>
			<table>
				<thead>
					<tr>
						<td style="width:15%;"></td>
						<td style="width:60%;">ZPOS（卡卡付收款宝）</td>
						<td style="width:25%;">传统POS</td>					
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>硬件价格</td>
						<td>无需押金，多种选择</td>
						<td>2000RMB左右</td>					
					</tr>
					<tr>
						<td>申请过程</td>
						<td>提供真实依据，迅速申请，合理费率</td>
						<td>流程复杂，验证检查复杂</td>					
					</tr>
					<tr>
						<td>审批时间</td>
						<td>两到三个工作日</td>
						<td>短则数周，多则几个月</td>					
					</tr>
					<tr>
						<td>收款地点</td>
						<td>24小时贴身移动收款</td>
						<td>指定地点收款</td>					
					</tr>
					<tr>
						<td>拓展功能</td>
						<td>多种支付方式：NFC支付、蓝牙POS支付，无卡支付；   多功能拓展：订单查询，结算查询，银行卡余额查询等</td>
						<td>只能刷卡支付</td>					
					</tr>
					<tr>
						<td>结算周期</td>
						<td>实时</td>
						<td>T+1</td>					
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<div class="footer">
		<div class="Copyright">Copyright 版权所有：成都卡卡付科技有限公司 All Right Reserve 蜀ICP备16003612号</div>
	</div> 
</body>
</html>