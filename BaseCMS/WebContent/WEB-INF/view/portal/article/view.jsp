<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>公司名-公告</title>
<%@ include file="/WEB-INF/view/portal/head.jsp"%>
<link rel="stylesheet" href="<c:url value="/css/portal/article.css"/>" />
</head>
<body>
	<div class="navbar-pay">
		<div class="nav_width">
			<div class="container">
				<a class="navbar-brand" href="<c:url value="/"/>"><img
					src="<c:url value="/images/portal/one/ikkpay_1.png"/>" class="logo" /></a>
				<ul>
					<li><a href="<c:url value="/portal"/>" class="active">主页</a></li>
					<li><a href="<c:url value="/portal/product"/>">产品展示</a></li>
					<li><a href="<c:url value="/portal/proCase"/>">成功案例</a></li>
					<li><a href="<c:url value="/portal/security"/>">安全保障</a></li>
					<li><a href="<c:url value="/portal/support"/>">技术支持</a></li>
					<li><a href="<c:url value="/portal/about"/>">关于我们</a></li>
				</ul>
			</div>
		</div>
	</div>

	<nav class="navbar navbar-default navbar-sec">
	<div class="container">
		<ul class="nav navbar-nav nav-sec">
			<li><a href="<c:url value="/"/>">主页/公告</a></li>
		</ul>
	</div>
	</nav>

	<div class="container sys-container">
		<div class="row">
			<div class="title-class">
				<div class="sys-title">${notice.title}</div>
				<div class="date">
					<fmt:formatDate value="${notice.createDate}" pattern="yyyy-MM-dd" />
				</div>
			</div>
			<hr />
			<div class="article">${notice.contents}</div>
		</div>
	</div>

	<div class="footer">
		<div class="Copyright">Copyright 版权所有：成都卡卡付科技有限公司 All Right
			Reserve 蜀ICP备16003612号</div>
	</div>

	<script>
		var conHeight = $('.sys-container').height();
		var nowHeight = $(window).height() - 263;
		if (conHeight < nowHeight) {
			$('.footer').addClass("navbar-fixed-bottom");
		}
	</script>
</body>
</html>
