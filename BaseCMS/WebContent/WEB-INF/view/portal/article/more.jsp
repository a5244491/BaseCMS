<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>公司名-公告</title>
<%@ include file="/WEB-INF/view/portal/head.jsp"%>
<link rel="stylesheet" href="<c:url value="/css/portal/article.css"/>" />
<style>
	.red {
	color: red;
	}
</style>
</head>

<body>
	<div class="navbar-pay">		
		<div class="nav_width">
			<div class="container">
				<a class="navbar-brand" href="<c:url value="/"/>"><img src="<c:url value="/images/portal/one/ikkpay_1.png"/>" class="logo"/></a>
				<ul>
					<li><a href="<c:url value="/portal"/>" class="active">主页</a></li>
					<li><a href="<c:url value="/portal/product"/>" >产品展示</a></li>
					<li><a href="<c:url value="/portal/proCase"/>" >成功案例</a></li>
					<li><a href="<c:url value="/portal/security"/>" >安全保障</a></li>
					<li><a href="<c:url value="/portal/support"/>" >技术支持</a></li>
					<li><a href="<c:url value="/portal/service"/>" >关于我们</a></li>
				</ul>  
			</div>
		</div>
	</div>
	
	<nav class="navbar navbar-default navbar-sec">
		<div class="container">
			<ul class="nav navbar-nav nav-sec">
				<li><a href="<c:url value="/"/>">主页/公告</a></li>
			</ul>
		</div>
	</nav>
	
	<div class="container sys-container" style="margin-top: 30px;">
		<div class="row">
			<div class="acctime-template" id="table_list">
				<div id="content">
				</div>
				 <div id="page" class="easyui-pagination" data-Options="pageList:[5,15,30,50,100],pageSize:15" style="background: #efefef; border: 1px solid #ccc;margin-bottom:40px;width: 100%"></div> 
			</div>
		</div>
	</div>
	
	<div class="footer">
		<div class="Copyright">Copyright 版权所有：成都卡卡付科技有限公司 All Right Reserve 蜀ICP备16003612号</div>
	</div> 
    
    <script>
    	var conHeight = $('.sys-container').height();
    	var nowHeight = $(window).height() - 263;
    	if(conHeight<nowHeight){
    		$('.footer').addClass("navbar-fixed-bottom");
    	}
    	
    	
    	$(function() {
    		queryPage();
    	});
    	function queryPage(pageNumber, pageSize) {
    		/* if (pageSize == null && pageNumber == null) {
    			$("#page").remove();
    			var content = '<div id="page" class="easyui-pagination"'+
    				'data-Options="pageList:[1,15,30,50,100],pageSize:15" style="background: #efefef; border: 1px solid #ccc;margin-bottom:40px;"></div>'
    					+ '  </div>';
    			$("#table_list").append(content);
    			if(navigator.appName == "Microsoft Internet Explorer" && navigator.appVersion .split(";")[1].replace(/[ ]/g,"")=="MSIE8.0") { 
    				 var divPage = document.createElement("div");
    				 divPage.innerHTML = '<div id="page" class="easyui-pagination"'+
    				'data-Options="pageList:[15,30,50,100],pageSize:15" style="background: #efefef; border: 1px solid #ccc;margin-bottom:40px;"></div>'
    				 +'  </div>';
    				 document.getElementById("table_list").appendChild(divPage);
    			 }
    		} */
    		$.ajax({
    			url : "../article/list",
    			type : "post",
    			data : {
    				pageNo : pageNumber,
    				pageSize : pageSize
    			},
    			cache : false,
    			success : function(html) {
    				$('#content').replaceWith(html);
    				var conDom = window.parent.parent.document
    						.getElementById("iframe");
    				$(conDom).height($('.content').height() + 100);
    				var botDom = window.parent.parent.document
    						.getElementById("bottom-navbar");
    				$(botDom).removeClass("navbar-fixed-bottom");
    			},
    			error : function() {
    				$('#content').replaceWith(
    						"<div style='color:red;'>你没有权限查看操作员数据，请联系管理员！</div>");
    			}
    		});
    	};
    	
    </script> 
    
    
</body>
</html>
