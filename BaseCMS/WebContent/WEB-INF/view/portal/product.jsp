<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>卡卡付-产品展示</title>
<%@ include file="/WEB-INF/view/portal/head.jsp"%>
<link rel="stylesheet" href="<c:url value="/css/portal/product.css"/>"/>
</head>
<body>
	<div class="navbar-pay">
		<div class="nav_width">
			<div class="container">
				<a class="navbar-brand" href="<c:url value="/"/>"><img
					src="<c:url value="/images/portal/one/ikkpay_1.png"/>" class="logo" /></a>
				<ul>
					<li><a href="<c:url value="/portal"/>" >主页</a></li>
					<li><a href="<c:url value="/portal/product"/>" class="active">产品展示</a></li>
					<li><a href="<c:url value="/portal/proCase"/>">成功案例</a></li>
					<li><a href="<c:url value="/portal/security"/>">安全保障</a></li>
					<li><a href="<c:url value="/portal/support"/>">技术支持</a></li>
					<li><a href="<c:url value="/portal/about"/>">关于我们</a></li>
				</ul>
			</div>
		</div>
	</div>
	<div class="topImg">
		<img src="<c:url value="/images/portal/product/product.png"/>" />
	</div>
	<div class="shoukuan">
		<div class="container">
			<hr />
			<img src="<c:url value="/images/portal/product/shoukuan.png"/>" class="titleImg" />
			<div class="row">
				<div class="col-md-3">
					<img src="<c:url value="/images/portal/product/NFC.png"/>" />
					<div class="pro-title">NFC支付</div>
					<div class="pro-con">无需开通网银，无需注册，芯片银行卡拍卡支付，国内首创移动安全支付方式</div>
				</div>
				<div class="col-md-3">
					<img src="<c:url value="/images/portal/product/identify.png"/>" />
					<div class="pro-title">认证支付</div>
					<div class="pro-con">无需开通网银，无需注册，银行卡四要素认证支付，实名银行卡安全支付方式</div>
				</div>
				<div class="col-md-3">
					<img src="<c:url value="/images/portal/product/network.png"/>" />
					<div class="pro-title">网银支付</div>
					<div class="pro-con">开通网银，即可付款，B2C：个人用户可以安全便捷处理各种网上交易，B2B：企业用户可以轻松解决企业间的对公结算</div>
				</div>
				<div class="col-md-3">
					<img src="<c:url value="/images/portal/product/chongzhi.png"/>" />
					<div class="pro-title">充值卡支付</div>
					<div class="pro-con">快速、安全、便捷在线支付方式，无需银行卡在任何公共网络环境安全支付，支持多种游戏充值卡、运营商卡</div>
				</div>
			</div>
			<div class="row" style="margin-top: 30px;">
				<div class="col-md-3">
					<a href="<%=path%>/zpos" target="_blank" style="cursor: pointer;">
						<img src="<c:url value="/images/portal/product/pos.png"/>" />
					</a>
					<div class="pro-title">ZPOS收款宝</div>
					<div class="pro-con">移动POS机，及时开通，NFC支付、无卡支付、蓝牙POS支付，实时收款，及时到账，费率优惠</div>
				</div>
				<div class="col-md-3">
					<img src="<c:url value="/images/portal/product/intrust.png"/>" />
					<div class="pro-title">协议支付</div>
					<div class="pro-con">付款方先授权收款方扣款，基于授权协议收款方主动发起扣款指令，商户方便、快速地进行收款
					</div>
				</div>
				<div class="col-md-3">
					<img src="<c:url value="/images/portal/product/protocol.png"/>" />
					<div class="pro-title">微信收款</div>
					<div class="pro-con">为用户提供微信扫码付款支付服务，为商户提供微信扫码收款支付服务</div>
				</div>
				<div class="col-md-3">
					<img src="<c:url value="/images/portal/product/juhe.png"/>" />
					<div class="pro-title">聚合支付</div>
					<div class="pro-con">整合各种支付方式，提供整体支付解决方案</div>
				</div>
			</div>
		</div>
	</div>

	<div class="fukuan">
		<div class="container">
			<hr />
			<img src="<c:url value="/images/portal/product/fukuan.png"/>" class="titleImg"/>
			<div class="row">
				<div class="col-md-4">
					<img src="<c:url value="/images/portal/product/fukuanTo.png"/>" />
				</div>
				<div class="col-md-4">
					<img src="<c:url value="/images/portal/product/piliang.png"/>" />
				</div>
				<div class="col-md-4">
					<img src="<c:url value="/images/portal/product/shishi.png"/>" />
				</div>
			</div>
		</div>
	</div>

	<div class="qita">
		<div class="container">
			<hr />
			<img src="<c:url value="/images/portal/product/qita.png"/>" class="titleImg"/>
			<div class="row">
				<div class="col-md-4">
					<img src="<c:url value="/images/portal/product/shiming.png"/>" />
					<div class="pro-title">银行卡实名认证</div>
				</div>
				<div class="col-md-4">
					<img src="<c:url value="/images/portal/product/nfcrenzheng.png"/>" />
					<div class="pro-title">NFC银行卡认证</div>
				</div>
				<div class="col-md-4">
					<img src="<c:url value="/images/portal/product/huafe.png"/>" />
					<div class="pro-title">话费充值</div>
				</div>
			</div>
		</div>
	</div>

	<div class="jiesuan">
		<div class="container">
			<hr />
			<img src="<c:url value="/images/portal/product/jiesuan.png"/>" class="titleImg"/>
			<div class="row">
				<div class="col-md-4">
					<img src="<c:url value="/images/portal/product/t+1.png"/>" />
					<div class="pro-con">当日交易，第二个工作日结算</div>
				</div>
				<div class="col-md-4">
					<img src="<c:url value="/images/portal/product/d+1.png"/>" />
					<div class="pro-con">当日交易，第二个自然日结算</div>
				</div>
				<div class="col-md-4">
					<img src="<c:url value="/images/portal/product/shisuan.png"/>" />
					<div class="pro-con">当日交易，当日结算</div>
				</div>
			</div>
		</div>
	</div>

	<div class="footer">
		<div class="Copyright">Copyright 版权所有：成都卡卡付科技有限公司 All Right
			Reserve 蜀ICP备16003612号</div>
	</div>
</body>
</html>
