<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="/tld/html-tags" prefix="html"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@ include file="/WEB-INF/view/head.jsp"%>
<style>
.table-bordered {
	text-align: left;
}

.table-bordered>tbody tr:nth-child(odd) {
	background: #fff;
}

.table-bordered>tbody tr td {
	padding-left: 40px;
	line-height: 30px;
}

.preTitle {
	width: 80px;
	display: -moz-inline-box;
	display: inline-block;
}

.upImg {
	margin-left: 95px;
	margin-top: 10px;
}

.item {
	margin: 15px;
}
</style>
</head>
<body>
	<div id="main"
		style="margin-left: auto; margin-right: auto; width: 980">
		<div class="table-container" id="content">
			<form class="form-inline form-index">
				<div id="uploadImg" class="item">
					<font color="red">*</font><span class="preTitle">用户名:</span> <input
						name="userName" id="userName" type="text" class="form-control"
						value="${user.userName }" disabled />
				</div>
				<div id="uploadImg" class="item">
					<font color="red">*</font><span class="preTitle">状态:</span> <input
						id="statusName" type="text" value="${user.statusName }"
						class="form-control" disabled> 
				</div>
				<div class="item">
					<font color="red">*</font> <span class="preTitle">姓名:</span> <input
						name="niceName" id="niceName" type="text" class="form-control"
						value="${user.niceName }" disabled />
				</div>
				<div class="item">
					<font color="red">*</font> <span class="preTitle">头像:</span>
					<div class="upImg">
						<img style='vertical-align: middle;' width='200' height='120'
							src='${user.faceUrl }' /> <input type='button'
							class='btn btn-default' onclick='openImg(this)' value='查看原图' />
					</div>
				</div>
			</form>
		</div>
	</div>
	<script type="text/javascript">
		function openImg(that) {
			var imgNode = $(that).prev();
			window.open($(imgNode).attr("src"));
		}
	</script>
</body>
</html>