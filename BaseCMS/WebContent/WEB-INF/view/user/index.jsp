<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="/tld/html-tags" prefix="html"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@ include file="/WEB-INF/view/head.jsp"%>
</head>
<body class="gray-bg">
	<div class="wrapper wrapper-content animated fadeInRight">
		<div class="row">
			<div class="col-sm-12">
				<div class="ibox float-e-margins">
					<div class="ibox-title">
						<form class="form-inline form-index" action="#" method="post"
							id="form">
							<div class="form-group">
								<label class="control-label">用户名</label> <input name="userName"
									id="userName" type="text" maxlength="20" class="form-control" />
							</div>
							<div class="form-group">
								<label class="control-label">昵称</label> <input name="niceName"
									id="niceName" type="text" maxlength="20" class="form-control" />
							</div>
							<div class="form-group">
								<label class="control-label">状态</label>
								<html:select cssClass="form-control" collection="userStatus"
									selectValue="${user.status }" name="status" id="status">
									<option value="">--全部--</option>
								</html:select>
							</div>
							<div class="form-group">
								<label class="control-label">更新时间</label> <input
									id="createDateStart" name="createDateStart"
									class="laydate-icon form-control layer-date"> - <input
									id="createDateEnd" name="createDateEnd"
									class="laydate-icon form-control layer-date">
							</div>

							<div class="form-group button-group" style="width: 100%;">

								<html:auth res="user/list">
									<input onclick="reload();" type="button" id="search" value="查询"
										class="btn btn-default">
								</html:auth>
								<html:auth res="user/add">
									<input type="button" id="new" value="新增"
										class="btn btn-default" onClick="add();">
								</html:auth>
								<input type="reset" id="reset" value="重置"
									class="btn btn-default">
							</div>
						</form>
					</div>
					<div class="ibox-content">
						<table id="data"
							class="table table-striped table-bordered table-hover dataTables-example">
							<thead>
								<tr>
									<th>用户名</th>
									<th>昵称</th>
									<th>状态</th>
									<th>更新时间</th>
									<th>操作</th>
								</tr>
							</thead>
							<tbody>
							</tbody>
						</table>

					</div>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		var table;
		function reload() {
			table.fnDraw();
		}
		var start = {
			elem : "#createDateStart",
			format : "YYYY-MM-DD 00:00:00",
			max : laydate.now(),
			istime : false,
			istoday : true,
			choose : function(datas) {
				end.min = datas;
				end.start = datas
			}
		};
		var end = {
			elem : "#createDateEnd",
			format : "YYYY-MM-DD 23:59:59",
			min : laydate.now(),
			max : "2099-06-16 23:59:59",
			istime : false,
			istoday : true,
			choose : function(datas) {
				start.max = datas
			}
		};
		laydate(start);
		laydate(end);
		$(document)
				.ready(
						function() {
							table = $("#data")
									.dataTable(
											{
												"bLengthChange" : false, //改变每页显示数据数量 
												"searching" : false,
												"bProcessing" : false, // 是否显示取数据时的那个等待提示
												"bServerSide" : true,//这个用来指明是通过服务端来取数据
												"sAjaxSource" : "user/list",//这个是请求的地址
												"fnServerData" : retrieveData,
												"columns" : [ {
													"data" : "userName",
													"defaultContent" : ""
												}, {
													"data" : "niceName",
													"defaultContent" : ""
												}, {
													"data" : "statusName",
													"defaultContent" : ""
												}, {
													"data" : "createDate",
													"defaultContent" : ""
												} ],
												"fnServerParams" : function(
														aoData) {
													aoData
															.push({
																"name" : "userName",
																"value" : $(
																		"#userName")
																		.val()
															});
													aoData
															.push({
																"name" : "niceName",
																"value" : $(
																		"#niceName")
																		.val()
															});
													aoData.push({
														"name" : "status",
														"value" : $("#status")
																.val()
													});
													aoData
															.push({
																"name" : "createDateStart",
																"value" : $(
																		"#createDateStart")
																		.val()
															});
													aoData
															.push({
																"name" : "createDateEnd",
																"value" : $(
																		"#createDateEnd")
																		.val()
															});
												},
												"columnDefs" : [
														// 将name列变为红色
														{
															"targets" : [ 0 ], // 目标列位置，下标从0开始
															"render" : function(
																	data, type,
																	full) { // 返回自定义内容
																return "<span style='color:red;'>"
																		+ data
																		+ "</span>";
															}
														},
														// 增加一列，包括删除和修改，同时将需要传递的数据传递到链接中
														{
															"targets" : [ 4 ], // 目标列位置，下标从0开始
															"data" : "userName", // 数据列名
															"render" : function(
																	data, type,
																	full) { // 返回自定义内容
																var modify = '<html:auth res="user/modify">'
																		+ '<a href="#" onclick="modify(\''
																		+ data
																		+ '\')">修改</a></html:auth>';
																var del = '<html:auth res="user/delete">'
																		+ '<a href="#" onclick="del(\''
																		+ data
																		+ '\')">删除</a></html:auth>';
																var view = '<html:auth res="user/view">'
																		+ '<a href="#" onclick="view(\''
																		+ data
																		+ '\')">详情</a></html:auth>';
																var resetPwd = '<html:auth res="user/resetPwd">'
																		+ '<a href="#" onclick="resetPwd(\''
																		+ data
																		+ '\')">密码重置</a></html:auth>';
																return del
																		+ "&nbsp;"
																		+ modify
																		+ "&nbsp;"
																		+ view
																		+ "&nbsp;"
																		+ resetPwd;
															}
														} ]
											// 获取数据的处理函数
											});
						});

		// 3个参数的名字可以随便命名,但必须是3个参数,少一个都不行
		function retrieveData(url, data, fnCallback) {
			$.ajax({
				url : url,//这个就是请求地址对应sAjaxSource
				data : {
					"iDisplayLength" : data.iDisplayLength,
					"iDisplayStart" : data.iDisplayStart,
					"userName" : data.userName,
					"niceName" : data.niceName,
					"status" : data.status,
					"createDateStart" : data.createDateStart,
					"createDateEnd" : data.createDateEnd
				},//这个是把datatable的一些基本数据传给后台,比如起始位置,每页显示的行数
				type : 'post',
				dataType : 'json',
				success : function(result) {
					fnCallback(result);//把返回的数据传给这个方法就可以了,datatable会自动绑定数据的
				},
				error : function(msg) {
				}
			});
		}

		function add() {
			var index = layer.open({
				type : 2,
				title : "用户新增",
				area : [ '800px', '400px' ],
				fix : false, //不固定
				maxmin : true,
				content : 'user/add'
			});
			layer.full(index);
		}
		function modify(id) {
			var index = layer.open({
				type : 2,
				title : "用户修改",
				area : [ '800px', '400px' ],
				fix : false, //不固定
				maxmin : true,
				content : 'user/modify?userName=' + id
			});
			layer.full(index);
		}
		function del(id) {
			//询问框
			layer.confirm('确定要删除么？', {
				btn : [ '确定', '取消' ]
			//按钮
			}, function() {
				$.ajax({
					url : "user/delete",
					data : {
						userName : id
					},
					type : 'post',
					contentType : 'application/x-www-form-urlencoded',
					encoding : 'UTF-8',
					cache : false,
					success : function(result) {
						if (result.success) {
							layer.alert('操作成功');
							reload();
						} else {
							layer.alert('操作失败,' + result.obj);
						}
					},
					error : function() {
						layer.alert('系统异常');
					}
				});
			}, function() {
				//取消动作
			});
		}
		function view(id) {
			var index = layer.open({
				type : 2,
				title : "用户详情",
				area : [ '800px', '400px' ],
				fix : false, //不固定
				maxmin : true,
				content : 'user/view?userName=' + id
			});
			layer.full(index);
		}
		function resetPwd(id) {
			//询问框
			layer.confirm('确定要重置该用户密码么？', {
				btn : [ '确定', '取消' ]
			//按钮
			}, function() {
				$.ajax({
					url : "user/resetPwd",
					data : {
						userName : id
					},
					type : 'post',
					contentType : 'application/x-www-form-urlencoded',
					encoding : 'UTF-8',
					cache : false,
					success : function(result) {
						if (result.success) {
							layer.alert('操作成功');
							reload();
						} else {
							layer.alert('操作失败,' + result.obj);
						}
					},
					error : function() {
						layer.alert('系统异常');
					}
				});
			}, function() {
				//取消动作
			});
		}
	</script>
</body>
</html>