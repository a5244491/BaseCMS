<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="/tld/html-tags" prefix="html"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@ include file="/WEB-INF/view/head.jsp"%>
</head>
<body class="gray-bg">
	<html:listPage ajaxUrl="accountMenu"
		columns="id,name,inputCode,eventType,url,sort,parentId,parentName,wxAccount,createDate,modifyDate"
		titles="编码,菜单名称,inputcode,事件类型,url,排序,父级菜单id,父级菜单名称,微信账号,创建时间,修改时间"
		queryIds="wxAccount-input,name-input,status-select|00_正常/01_关闭,createDate_start|createDate_end-date,modifyDate_start|modifyDate_end-date"
		queryTitles="公众号,公众号名称,状态,创建时间,修改时间">
	</html:listPage>
</body>
</html>